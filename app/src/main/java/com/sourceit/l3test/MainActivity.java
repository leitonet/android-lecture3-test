package com.sourceit.l3test;

import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.EditText;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

    public static final int ID = 1;
    Set<String> colorsArray = new HashSet<>();
    Iterator <String> iterator = colorsArray.iterator();;
    View view;

    Handler handler = new Handler(){
        @Override
        public void dispatchMessage(Message msg) {
            if(msg.what == ID){

                if(iterator.hasNext()) {
                    view.setBackgroundColor(Color.parseColor(iterator.next()));
                }else {
                    iterator = colorsArray.iterator();
                    view.setBackgroundColor(Color.parseColor(iterator.next()));
                }
                handler.sendEmptyMessageDelayed(ID, 1500);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        generateColors();
        view =  findViewById(R.id.activity_main);
    }

    @Override
    protected void onResume() {
        super.onResume();
        handler.sendEmptyMessage(ID);
    }

    @Override
    protected void onPause() {
        super.onPause();
        handler.removeMessages(ID);
    }

    public void generateColors(){
        colorsArray.add("#dce775");
        colorsArray.add("#ffa726");
        colorsArray.add("#f44336");
        colorsArray.add("#ec407a");
        colorsArray.add("#ab47bc");
        colorsArray.add("#7e57c2");
        colorsArray.add("#5c6bc0");
        colorsArray.add("#e0e0e0");
        colorsArray.add("#4dd0e1");
        colorsArray.add("#4db6ac");
    }

}
